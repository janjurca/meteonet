from diffusers.models import AutoencoderKL
import torch
import os 
from PIL import Image
import numpy as np
import sys
from matplotlib import pyplot as plt

#stabilityai/sd-vae-ft-mse
vae = AutoencoderKL.from_pretrained("stabilityai/sdxl-vae")

image = Image.open(sys.argv[1])
image = image.resize((256,256))
image = image.convert("RGB")
image.save("orig.png")
image = np.array(image)
image = image.transpose(2,0,1)
image = image.astype(np.float32)
image = torch.from_numpy(image)
image = image.unsqueeze(0)
image = image / 255.0
image = image - 0.5
image = image * 2.0
print(image.shape)
z = vae(image)
z = z.sample
print(z.shape)
z = z.cpu().detach().numpy()
z = z.squeeze()
z = z.transpose(1,2,0)
z = z / 2.0
z = z + 0.5
z = z * 255.0
print(z)
z = z.astype(np.uint8)

img = Image.fromarray(z)

img = img.convert("L")
img.save("test.png")

plt.hist(z.flatten(), bins=256)
plt.show()

