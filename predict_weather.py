import argparse
import os
from train_diffusion import MeteoNet
from srgan.models import Generator
from srgan.utils import convert_image, imagenet_mean, imagenet_std
import torch
import glob 
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_path', type=str, required=True, help='path to the input images')
    parser.add_argument('--meteonet_path', type=str, required=True, help='path to the trained MeteoNet model')
    parser.add_argument('--srnet_path', type=str, required=True, help='path to the trained SRGAN model')
    parser.add_argument('--device', type=str, default='cpu', help='device to use')
    parser.add_argument('--diffusion_steps', type=int, default=20, help='number of diffusion steps')
    parser.add_argument('--size', type=int, default=256, help='size of the input images')

    parser.add_argument('--future_steps', type=int, default=12, help='number of future steps to predict')
    args = parser.parse_args()

    meteonet = MeteoNet.load_from_checkpoint(args.meteonet_path, map_location=torch.device('cpu'))
    meteonet = meteonet.to(args.device)
    meteonet.eval()

    srnet = torch.load(args.srnet_path, map_location=torch.device('cpu'))
    print(srnet.keys())
    if "generator" in srnet:
        srnet = srnet["generator"]
    elif "model" in srnet:
        srnet = srnet["model"]
    srnet = srnet.to(args.device)
    srnet.eval()

    # search for cintuuous sequence of images in the input path
    files = sorted(list(glob.glob(os.path.join(args.input_path, "*.png"))))
    sequence = []
    for i, file in enumerate(files):
        if not sequence:
            sequence.append(file)
            continue
        else:
            if int(file.split("/")[-1].split(".")[0]) == int(sequence[-1].split("/")[-1].split(".")[0]) + 600:
                sequence.append(file)
            else:
                sequence = []
        if len(sequence) == 8 + args.future_steps:
            break
    if len(sequence) < 8 + args.future_steps:
        print("Not enough images in the input path")
        return
    
    # load images
    x = []
    for file in sequence[:8]:
        x.append(torch.tensor(np.array(Image.open(file).convert("L").resize((args.size, args.size)), dtype=np.float32)/255.0, dtype=torch.float32))

    x = torch.stack(x)
    xt = x[:-1,...].unsqueeze(0)
    xt = xt.to(args.device)
    print(xt.shape)
    samples = meteonet.ddim_sampler(steps=args.diffusion_steps)(xt)
    frames = torch.cat([xt.to(samples[-1].device), samples], dim=1)
    # Convert torch tensor to numpy
    frames_np = frames.cpu().squeeze()
    
    prediction_raw = frames_np[7,...]
    prediction = np.clip(prediction_raw, 0, None)

    prediction = prediction * 255
    prediction = prediction.numpy().astype(np.uint8)

    prediction = Image.fromarray(prediction)

    prediction_raw = (prediction_raw - imagenet_mean.mean()) / imagenet_std.mean()
    print(prediction_raw.shape)
    # do a super resolution
    sr_img_srnet = srnet(prediction_raw.unsqueeze(0).unsqueeze(0).to(args.device))
    sr_img_srnet = sr_img_srnet.squeeze(0).cpu().detach()
    sr_prediction = convert_image(sr_img_srnet, source='[-1, 1]', target='pil')
    print(prediction.size)


    original = Image.open(sequence[7])
    original = original.convert("L")
    original = original.resize((512,512))

    original_raw = original.resize((args.size, args.size))    
    original_raw = convert_image(original_raw, source='pil', target='imagenet-norm').unsqueeze(0).to(args.device)

    sr_original = srnet(original_raw)
    sr_original = sr_original.squeeze(0).cpu().detach()
    sr_original = convert_image(sr_original, source='[-1, 1]', target='pil')
    
    # plot orginal and predicted image
    fig, ax = plt.subplots(1,4)
    ax[0].imshow(prediction, cmap='gray')
    ax[1].imshow(sr_prediction, cmap='gray')
    ax[2].imshow(original, cmap='gray')
    ax[3].imshow(sr_original, cmap='gray')
    plt.show()

    
if __name__ == '__main__':
    main()