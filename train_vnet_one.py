from lightning.pytorch import cli_lightning_logo
from lightning.pytorch.cli import LightningCLI
from lightning.pytorch.callbacks import ModelCheckpoint


from PIL import Image
from nets.meteoset import MeteoDataModule
from nets.meteonet import MeteoNet

Image.MAX_IMAGE_PIXELS = 933120000



def cli_main():
    checkpoint_callback = ModelCheckpoint(
                dirpath='./checkpoints',
                monitor='val_loss',
                save_top_k=2
            )
    cli = LightningCLI(
        MeteoNet,
        MeteoDataModule,
        seed_everything_default=1234,
        run=False,  # used to de-activate automatic fitting.
        trainer_defaults={"callbacks": [checkpoint_callback], "max_epochs": 10},
        save_config_kwargs={"overwrite": True},
    )
    cli.trainer.fit(cli.model, datamodule=cli.datamodule, )
    cli.trainer.test(datamodule=cli.datamodule)
    predictions = cli.trainer.predict(datamodule=cli.datamodule)
    print(predictions[0])


if __name__ == "__main__":
    cli_lightning_logo()
    cli_main()
