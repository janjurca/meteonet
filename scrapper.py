import requests
import sys
import os
from PIL import Image
OUTPUT_DIR = sys.argv[1]

Image.MAX_IMAGE_PIXELS = 933120000

# Get available timestamps
rt = requests.get('https://api.rainviewer.com/public/maps.json')
data = rt.json()

for timestamp in data:
    timestamp = str(timestamp)
    if not os.path.exists(os.path.join(OUTPUT_DIR, timestamp + ".png")):
        print("Downloading", timestamp)
        r = requests.get('https://tilecache.rainviewer.com/v2/radar/' + timestamp + '/24000/0/0_1.png')
        with open(os.path.join(OUTPUT_DIR, timestamp + ".png"), 'wb') as f:
            f.write(r.content)
        print("Done",timestamp)
        img = Image.open(os.path.join(OUTPUT_DIR, timestamp + ".png"))
        img = img.convert("L")
        img.save(os.path.join(OUTPUT_DIR, timestamp + ".png"))