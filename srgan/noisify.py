from PIL import Image, ImageFilter
import numpy as np
import matplotlib.pyplot as plt
import sys

# Load the image
img = Image.open(sys.argv[1])

# Convert the image to grayscale
img = img.convert("L")

# Convert the image to a NumPy array
img_array = np.asarray(img)

# Define the standard deviation of the Gaussian noise
sigma = 5

# Generate Gaussian noise
noise = np.random.normal(0, sigma, img_array.shape)

# Add the Gaussian noise to the image
noisy_img_array = img_array + noise

# Clip the values to be in the valid range
noisy_img_array = np.clip(noisy_img_array, 0, 255)

# Convert the NumPy array back to an image
noisy_img = Image.fromarray(noisy_img_array.astype('uint8'))

noisy_img = noisy_img.resize((int(noisy_img.width / 2), int(noisy_img.height / 2)), Image.BICUBIC)

noisy_img1 = noisy_img.filter(ImageFilter.GaussianBlur(radius=1))
noisy_img2 = noisy_img.filter(ImageFilter.GaussianBlur(radius=2))
noisy_img3 = noisy_img.filter(ImageFilter.GaussianBlur(radius=3))

# Display the noisy images in one plot
fig, ax = plt.subplots(1, 5)
ax[0].imshow(img, cmap='gray')
ax[0].set_title('Original Image')
ax[1].imshow(noisy_img, cmap='gray')
ax[1].set_title('Noisy Image')
ax[2].imshow(noisy_img1, cmap='gray')
ax[2].set_title('Noisy Image 1')
ax[3].imshow(noisy_img2, cmap='gray')
ax[3].set_title('Noisy Image 2')
ax[4].imshow(noisy_img3, cmap='gray')
ax[4].set_title('Noisy Image 3')
plt.show()

