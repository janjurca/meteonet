import wandb
import time
import torch.backends.cudnn as cudnn
import torch
from torch import nn
from models import SRResNet
from datasets import SRDataset
from utils import *
import os
from PIL import Image
import torchvision
import numpy as np



# Data parameters
data_folder = os.environ.get("DATA_FOLDER", '/home/jjurca/Dataset/export_1024_hc')
crop_size = int(os.environ.get("CROP_SIZE", 96))  # crop size of target HR images
scaling_factor = int(os.environ.get("SCALING_FACTOR", 4))  # the scaling factor for the generator; the input LR images will be downsampled from the target HR images by this factor

# Model parameters
large_kernel_size = 15  # kernel size of the first and last convolutions which transform the inputs and outputs
small_kernel_size = 5  # kernel size of all convolutions in-between, i.e. those in the residual and subpixel convolutional blocks
n_channels = 64  # number of channels in-between, i.e. the input and output channels for the residual and subpixel convolutional blocks
n_blocks = 24  # number of residual blocks

# Learning parameters
checkpoint = None  # path to model checkpoint, None if none
batch_size = int(os.environ.get("BATCH_SIZE", 32))  # batch size
start_epoch = 0  # start at this epoch
iterations = 1e6  # number of training iterations
workers = 4  # number of workers for loading data in the DataLoader
print_freq = 10  # print training status once every __ batches
lr = 1e-4  # learning rate
grad_clip = None  # clip if gradients are exploding

try:
    device = torch.device("mps" if torch.backends.mps.is_available() else "cpu")
except RuntimeError:
    print("MPS not available")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


cudnn.benchmark = True

selected_image_indexes = None

def main():
    """
    Training.
    """
    global start_epoch, epoch, checkpoint

    # Initialize model or load checkpoint
    if checkpoint is None:
        model = SRResNet(large_kernel_size=large_kernel_size, small_kernel_size=small_kernel_size,
                         n_channels=n_channels, n_blocks=n_blocks, scaling_factor=scaling_factor)
        # Initialize the optimizer
        optimizer = torch.optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()),
                                     lr=lr)

    else:
        checkpoint = torch.load(checkpoint)
        start_epoch = checkpoint['epoch'] + 1
        model = checkpoint['model']
        optimizer = checkpoint['optimizer']

    # Move to default device
    model = model.to(device)
    criterion = nn.MSELoss().to(device)

    # Custom dataloaders
    train_dataset = SRDataset(data_folder,
                              split='train',
                              crop_size=crop_size,
                              scaling_factor=scaling_factor,
                              lr_img_type='imagenet-norm',
                              hr_img_type='[-1, 1]')
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=workers,
                                               pin_memory=True)  # note that we're passing the collate function here

    # Total number of epochs to train for
    epochs = int(iterations // len(train_loader) + 1)
    # Epochs
    for epoch in range(start_epoch, epochs):
        # One epoch's training
        loss = train(train_loader=train_loader,
              model=model,
              criterion=criterion,
              optimizer=optimizer,
              epoch=epoch)

        infered_images = infere_and_log_n_images(model, 10, train_dataset)
        wandb.log({"loss": loss, "infered_images": infered_images})

        # Save checkpoint
        torch.save({'epoch': epoch,
                    'model': model,
                    'optimizer': optimizer},
                   'checkpoint_srresnet.pth.tar')

@torch.no_grad()
def infere_and_log_n_images(generator, n, dataset):
    global selected_image_indexes
    if selected_image_indexes is None:
        selected_image_indexes = np.random.randint(0, len(dataset), n)

    generator.eval()
    infered = []
    for i in selected_image_indexes:
        original, lr_img, hr_img = dataset.get_original(i)
        original_degraded = original.resize((original.size[0] // scaling_factor,original.size[1] // scaling_factor), Image.BICUBIC)
        original_degraded = original_degraded.resize(original.size, Image.BICUBIC)
        lr_img = lr_img.to(device)
        hr_img = hr_img.to(device)
        sr_img = generator(lr_img.unsqueeze(0)).squeeze(0)
        lr_img, sr_img, hr_img = lr_img.cpu(), sr_img.cpu(), hr_img.cpu()
        sr_img = convert_image(sr_img, source='[-1, 1]', target='pil')
        
        #convert images to tensors 
        original_degraded = torchvision.transforms.ToTensor()(original_degraded)
        sr_img = torchvision.transforms.ToTensor()(sr_img)
        original = torchvision.transforms.ToTensor()(original)
        #make grid of those images
        grid = torchvision.utils.make_grid([original_degraded, sr_img, original], nrow=3)
        infered.append(wandb.Image(grid, caption=f"lr, sr, hr image {i}"))

    generator.train()
    return infered



def train(train_loader, model, criterion, optimizer, epoch):
    """
    One epoch's training.

    :param train_loader: DataLoader for training data
    :param model: model
    :param criterion: content loss function (Mean Squared-Error loss)
    :param optimizer: optimizer
    :param epoch: epoch number
    """
    model.train()  # training mode enables batch normalization

    batch_time = AverageMeter()  # forward prop. + back prop. time
    data_time = AverageMeter()  # data loading time
    losses = AverageMeter()  # loss

    start = time.time()

    # Batches
    for i, (lr_imgs, hr_imgs) in enumerate(train_loader):
        data_time.update(time.time() - start)

        # Move to default device
        lr_imgs = lr_imgs.to(device)  # (batch_size (N), 3, 24, 24), imagenet-normed
        hr_imgs = hr_imgs.to(device)  # (batch_size (N), 3, 96, 96), in [-1, 1]

        # Forward prop.
        sr_imgs = model(lr_imgs)  # (N, 3, 96, 96), in [-1, 1]

        # Loss
        loss = criterion(sr_imgs, hr_imgs)  # scalar

        # Backward prop.
        optimizer.zero_grad()
        loss.backward()

        # Clip gradients, if necessary
        if grad_clip is not None:
            clip_gradient(optimizer, grad_clip)

        # Update model
        optimizer.step()

        # Keep track of loss
        losses.update(loss.item(), lr_imgs.size(0))

        # Keep track of batch time
        batch_time.update(time.time() - start)

        # Reset start time
        start = time.time()

        # Print status
        if i % print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]----'
                  'Batch Time {batch_time.val:.3f} ({batch_time.avg:.3f})----'
                  'Data Time {data_time.val:.3f} ({data_time.avg:.3f})----'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(epoch, i, len(train_loader),
                                                                    batch_time=batch_time,
                                                                    data_time=data_time, loss=losses))
    del lr_imgs, hr_imgs, sr_imgs  # free some memory since their histories may be stored
    return losses.avg

if __name__ == '__main__':
    run = wandb.init(project="meteo_srresnet_orig", config={
        "batch_size": batch_size,
        "crop_size": crop_size,
        "scaling_factor": scaling_factor,
        "large_kernel_size": large_kernel_size,
        "small_kernel_size": small_kernel_size,
        "n_channels": n_channels,
        "n_blocks": n_blocks,
        "lr": lr,
        "grad_clip": grad_clip,
        "iterations": iterations,
        "workers": workers,
        "print_freq": print_freq,
        "data_folder": data_folder,
    })
    main()
