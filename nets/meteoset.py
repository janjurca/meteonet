import torch
import torch.utils.data
import torch.nn.functional as F
from torch import nn
from torch.utils.data import DataLoader, random_split
import random
import glob
from os import path
from PIL import Image
import os
import numpy as np
from lightning.pytorch import LightningDataModule
from torchvision import transforms

from PIL import ImageFilter

class MeteoDataset(torch.utils.data.Dataset):
    FILE_APPEND = ".png"
    def __init__(self, root, portion=1, input_sequence_len=8, output_sequence_len=0, image_size_in=512, image_size_out=512, return_original=False, is3D=False):
        self.root = root
        self.input_sequence_len = input_sequence_len
        self.output_sequence_len = output_sequence_len
        self.image_size_in = image_size_in
        self.image_size_out = image_size_out
        self.return_original = return_original
        self.is3D = is3D
        
        tiles_dirs = glob.glob(path.join(root, "*"))
        tiles = {} 
        for tile_dir in tiles_dirs:
            if path.isdir(tile_dir):
                tile = path.basename(tile_dir)
                tiles[tile] = []
                for file in glob.glob(path.join(tile_dir, f"*{self.FILE_APPEND}")):
                    tiles[tile].append(file)
                tiles[tile].sort()
                
        self.sequences = []
        diference = 600
        for tile in tiles:
            for i, file in enumerate(tiles[tile]):
                sequence = []
                file_timestamp = int(path.basename(file).split(".")[0])
                for j, file2 in enumerate(tiles[tile][i+1:]):
                    file2_timestamp = int(path.basename(file2).split(".")[0])
                    if file2_timestamp == file_timestamp+(j+1)*diference:
                        sequence.append(file2)
                    else:
                        break
                    if len(sequence) == self.input_sequence_len + self.output_sequence_len:
                        break
                if len(sequence) == self.input_sequence_len + self.output_sequence_len:
                    self.sequences.append(tuple(sequence))
        if portion < 0:
            self.data = self.sequences[int(len(self.sequences)*(1+portion)):]
        else:
            self.data = self.sequences[:int(len(self.sequences)*portion)]
        random.shuffle(self.data)

    def cut_dataset(self, n,first=True):
        if first:
            self.data = self.data[:n]
        else:
            self.data = self.data[n:]
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        sequence = self.data[index]
        query = []
        gts = []
        for i, file in enumerate(sequence):
            with Image.open(file) as img:
                img = img.convert("L")
                if i < self.input_sequence_len:
                    img = img.resize((self.image_size_in,self.image_size_in))
                else:
                    img = img.resize((self.image_size_out,self.image_size_out))
                img = np.array(img, dtype=np.float32)
                img = torch.from_numpy(img)
                img = img.float() / 255.0
                if i >= self.input_sequence_len:
                    gts.append(img)
                else:
                    query.append(img)
        query = torch.stack(query)
        gts = torch.stack(gts)
        return query, gts

class DiffusionMeteoDataset(MeteoDataset):
    def __getitem__(self, index):
        sequence = self.data[index]
        query = []
        rotation = random.choice([0,90,180,270])
        h_flip = random.choice([True, False])
        v_flip = random.choice([True, False])
        if random.random() < 0.1:
            sequence = reversed(sequence)

        for i, file in enumerate(sequence):
            with Image.open(file) as img:
                img = img.convert("L")
                img = img.rotate(rotation)
                if h_flip:
                    img = img.transpose(Image.FLIP_LEFT_RIGHT)
                if v_flip:
                    img = img.transpose(Image.FLIP_TOP_BOTTOM)
                img = img.resize((self.image_size_in,self.image_size_in))
                img = np.array(img, dtype=np.float32)
                img = torch.from_numpy(img)
                img = img.float() / 255.0
                img = img - 0.5
                img = img * 2.0
                query.append(img)
        query = torch.stack(query)
        if self.is3D:
            query = query.unsqueeze(0)
        if self.return_original:
            return query, sequence
        return query

class LatentMeteoDataset(MeteoDataset):
    FILE_APPEND = ".pt"
    def __getitem__(self, index):
        sequence = self.data[index]
        query = []
        
        for i, file in enumerate(sequence):
            z = torch.load(file).squeeze(0)
            query.append(z)
        query = torch.cat(query, dim=0)
        return query



class MeteoDataModule(LightningDataModule):
    def __init__(self, dataset_path: str, batch_size: int = 32, num_workers: int = 1, input_sequence_len: int = 16, output_sequence_len: int = 1, image_size_in: int = 512  , image_size_out: int = 512 , dataset_cls = None, return_original=False, is3D=False):
        super().__init__()
        if dataset_cls is None:
            dataset_cls = MeteoDataset
        elif dataset_cls == "diffusion":
            dataset_cls = DiffusionMeteoDataset

        self.test = dataset_cls(root=dataset_path, portion=-0.1, input_sequence_len=input_sequence_len, output_sequence_len=output_sequence_len, image_size_in=image_size_in, image_size_out=image_size_out, return_original=return_original, is3D=is3D)
        self.train =  dataset_cls(root=dataset_path, portion=0.9, input_sequence_len=input_sequence_len, output_sequence_len=output_sequence_len, image_size_in=image_size_in, image_size_out=image_size_out, return_original=return_original, is3D=is3D)
        self.val = dataset_cls(root=dataset_path, portion=-0.1, input_sequence_len=input_sequence_len, output_sequence_len=output_sequence_len, image_size_in=image_size_in, image_size_out=image_size_out, return_original=return_original, is3D=is3D)
        self.val.cut_dataset(10, first=True)
        self.test.cut_dataset(10, first=False)
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.collate = None 

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, num_workers=self.num_workers, collate_fn=self.collate)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.batch_size, num_workers=self.num_workers, collate_fn=self.collate)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, num_workers=self.num_workers, collate_fn=self.collate)

    def predict_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, num_workers=self.num_workers, collate_fn=self.collate)


class SRMeteoDataset(torch.utils.data.Dataset):
    def __init__(self, root, portion:float=1, image_size_hr:int=128, crop:int=2, split: str="train", augment:bool=False, rgb:bool=False):
        self.root = root
        self.image_size_hr = image_size_hr
        self.crop = crop
        self.split = split
        self.augment = augment
        self.rgb = rgb
        
        self.images = glob.glob(path.join(root,"*" ,"*"))
        
        if portion < 0:
            self.data = self.images[int(len(self.images)*(1+portion)):]
        else:
            self.data = self.images[:int(len(self.images)*portion)]
        
        self.transform = transforms.Compose([
            transforms.ToTensor(),
        ])
        random.shuffle(self.data)

    def cut_dataset(self, n,first=True):
        if first:
            self.data = self.data[:n]
        else:
            self.data = self.data[n:]
        
    def __len__(self):
        return len(self.data)
    

    def __getitem__(self, index):
        hr = Image.open(self.data[index])
        lr = Image.open(self.data[index])
        
        if self.rgb:
            hr = hr.convert("RGB")
            lr = lr.convert("RGB")
        else:
            hr = hr.convert("L")
            lr = lr.convert("L")

        # Crop
        if self.split == 'train':
            # Take a random fixed-size crop of the image, which will serve as the high-resolution (HR) image
            left = random.randint(1, hr.width - self.image_size_hr)
            top = random.randint(1, hr.height - self.image_size_hr)
            right = left + self.image_size_hr
            bottom = top + self.image_size_hr
            hr = hr.crop((left, top, right, bottom))
            lr = lr.crop((left, top, right, bottom))
            

        #lr = lr.resize((self.image_size_lr,self.image_size_lr))
        lr = lr.resize((hr.size[0]//self.crop,hr.size[1]//self.crop))
        rnd = random.random()
        if rnd < 0.5 and self.augment is True:
            lr = lr.filter(ImageFilter.GaussianBlur(1))



        #image = image.transpose(2,0,1)
        #image = image.astype(np.float32)
        #image = torch.from_numpy(image)
        #image = image.unsqueeze(0)
        #image = image / 255.0
        #image = image - 0.5
        #image = image * 2.0

        hr = np.array(hr, dtype=np.float32)
        hr = hr.transpose(2,0,1)
        hr = torch.from_numpy(hr)
        hr = hr.float() / 255.0
        hr = hr - 0.5
        hr = hr * 2.0

        lr = np.array(lr, dtype=np.float32)
        lr = lr.transpose(2,0,1)
        lr = torch.from_numpy(lr)
        lr = lr.float() / 255.0
        lr = lr - 0.5
        lr = lr * 2.0
        
        #hr = self.transform(hr)
        #lr = self.transform(lr)
        
        return lr, hr

class SRMeteoDataModule(LightningDataModule):
    def __init__(self, dataset_path: str, batch_size: int = 32, val_batch_size:int=6, num_workers: int = 1, image_size_hr: int = 128, crop: int = 2):
        super().__init__()
        self.test = SRMeteoDataset(root=dataset_path, portion=-0.25, image_size_hr=image_size_hr, crop=crop, split="test")
        self.train =  SRMeteoDataset(root=dataset_path, portion=0.75, image_size_hr=image_size_hr, crop=crop, split="train")
        self.val = SRMeteoDataset(root=dataset_path, portion=-0.25, image_size_hr=image_size_hr, crop=crop, split="val")
        self.val.cut_dataset(10, first=True)
        self.test.cut_dataset(10, first=False)
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.val_batch_size = val_batch_size

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, num_workers=self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.val_batch_size, num_workers=self.num_workers)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.val_batch_size, num_workers=self.num_workers)

    def predict_dataloader(self):
        return DataLoader(self.test, batch_size=self.val_batch_size, num_workers=self.num_workers)



class VAEMeteoDataModule(LightningDataModule):
    def __init__(self, dataset_path: str, batch_size: int = 4, val_batch_size:int=6, num_workers: int = 1, image_size_hr: int = 128, crop: int = 1):
        super().__init__()
        self.test = SRMeteoDataset(root=dataset_path, portion=-0.05, image_size_hr=image_size_hr, crop=crop, split="train", rgb=True)
        self.train = SRMeteoDataset(root=dataset_path, portion=0.95, image_size_hr=image_size_hr, crop=crop, split="train", rgb=True)
        self.val = SRMeteoDataset(root=dataset_path, portion=-0.05, image_size_hr=image_size_hr, crop=crop, split="train", rgb=True)
        self.val.cut_dataset(10, first=True)
        self.test.cut_dataset(10, first=False)
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.val_batch_size = val_batch_size

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, num_workers=self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.val_batch_size, num_workers=self.num_workers, shuffle=False)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.val_batch_size, num_workers=self.num_workers, shuffle=False)

    def predict_dataloader(self):
        return DataLoader(self.test, batch_size=self.val_batch_size, num_workers=self.num_workers, shuffle=False)

