
import os
import torch
import torch.nn.functional as F
from PIL import Image
import os
import numpy as np
from lightning.pytorch import LightningModule
from nets.vnet import VNet


class MeteoNet(LightningModule):
    def __init__(self, learning_rate: float = 0.0001, input_sequence_len=16, output_seuqence_len=1):
        super().__init__()
        self.save_hyperparameters()
        self.model = VNet(elu=False, nll=True, inCH=int(input_sequence_len), outCH=int(output_seuqence_len))

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        return self._common_step(batch, batch_idx, "train")[0]

    def validation_step(self, batch, batch_idx):
        loss, output = self._common_step(batch, batch_idx, "val")
        # set dimensionaly same as batch
        if batch_idx == 0:
            output = output.view(batch[1].shape)
            imgs = []
            # extract first image from output 
            output = output[0]
            imgs = []
            for i, img in enumerate(output):
                img = img.unsqueeze(0)
                os.makedirs("output", exist_ok=True)
                img= img.detach().cpu()
                img = img.squeeze(0)
                img = img.numpy()
                img = np.array(img*255, dtype=np.uint8)
                image = Image.fromarray(img)
                imgs.append(image)
            gts = []
            for i, img in enumerate(batch[1][0]):
                img = img.unsqueeze(0)
                os.makedirs("output", exist_ok=True)
                img= img.detach().cpu()
                img = img.squeeze(0)
                img = img.numpy()
                img = np.array(img*255, dtype=np.uint8)
                image = Image.fromarray(img)
                gts.append(image)
            self.logger.log_image(key="samples", images=imgs + gts, step=self.global_step, caption=[f"output {i}" for i in range(len(imgs))] + [f"gt {i}" for i in range(len(gts))])
        return loss

    def test_step(self, batch, batch_idx):
        loss, output = self._common_step(batch, batch_idx, "test")
        return loss

    def predict_step(self, batch, batch_idx, dataloader_idx=None):
        q, _ = batch
        return self(q)

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.hparams.learning_rate)


    def _common_step(self, batch, batch_idx, stage: str):
        q, gt = batch
        output = self(q)
        gt = gt.view(gt.numel())
        loss = F.mse_loss(gt,output )
        self.log(f"{stage}_loss", loss, on_step=True)
        return loss, output


