from torchsr.models import ninasr_b0, rcan, rdn, ninasr_b1
from torchvision.transforms.functional import to_pil_image, to_tensor
import sys
from PIL import Image
import matplotlib.pyplot as plt


# Load a low-resolution image
lr = Image.open(sys.argv[1])
lr = lr.convert("RGB")
lr = lr.resize((512, 512))
ratio = lr.size[0] / lr.size[1]
base = 512
lr = lr.resize((int(base*ratio), base))
# Download a pretrained NinaSR model
model = ninasr_b0(scale=2, pretrained=True)

# Run the Super-Resolution model
lr_t = to_tensor(lr).unsqueeze(0)
print("Running Super-Resolution model...", flush=True)
sr_t = model(lr_t)
print("Done!", flush=True)
sr = to_pil_image(sr_t.squeeze(0))
sr = sr.convert("L")
lr.show()
sr.show()

model = ninasr_b0(scale=4, pretrained=True)
sr_4 = to_pil_image(model(lr_t).squeeze(0))
sr_4 = sr_4.convert("L")
# plot results
fig, axs = plt.subplots(1, 3)
axs[0].imshow(lr)
axs[1].imshow(sr)
axs[2].imshow(sr_4)
plt.show()
