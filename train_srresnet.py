import torch
from torch import nn

from lightning.pytorch import LightningModule
from lightning.pytorch.cli import LightningCLI

from models import SRResNet
from nets.meteoset import SRMeteoDataModule


class MeteoSR(LightningModule):
    def __init__(self, learning_rate: float = 0.0001, scaling_factor = 2, beta = 0.001,grad_clip = None,
                 large_kernel_size_g: int = 9, small_kernel_size_g: int = 3, n_channels_g: int = 64, n_blocks_g: int = 16,
                 ):
        super().__init__()
        self.save_hyperparameters()
        self.model = SRResNet(large_kernel_size=large_kernel_size_g,
                              small_kernel_size=small_kernel_size_g,
                              n_channels=n_channels_g,
                              n_blocks=n_blocks_g,
                              scaling_factor=scaling_factor)

        self.loss = nn.MSELoss(reduction="sum")

    def forward(self, x):
        x = self.model(x)
        return x


    def training_step(self, batch, batch_idx):
        return self._common_step(batch, batch_idx, "train")[0]

    def validation_step(self, batch, batch_idx):
        loss, out = self._common_step(batch, batch_idx, "val")

        if batch_idx == 0:
            images = []
            for image in out:
                images.append(image)
            self.logger.log_image(key="sr", images=images, step=self.global_step)

        return loss


    def configure_optimizers(self):
        lr = self.hparams.learning_rate
        return torch.optim.Adam(params=filter(lambda p: p.requires_grad, self.model.parameters()),
                                     lr=lr)

    def _common_step(self, batch, batch_idx, stage: str):
        lr_imgs, hr_imgs = batch
        sr_imgs = self(lr_imgs)
        loss = self.loss(sr_imgs, hr_imgs)
        self.log(f"{stage}_loss", loss, prog_bar=True)
        return loss, sr_imgs


def cli_main():
    cli = LightningCLI(
        MeteoSR,
        SRMeteoDataModule,
        seed_everything_default=1234,
        run=False,  # used to de-activate automatic fitting.
        trainer_defaults={"max_epochs": 10},
        save_config_kwargs={"overwrite": True},
    )
    cli.trainer.fit(cli.model, datamodule=cli.datamodule, )
    #cli.trainer.test(datamodule=cli.datamodule)


if __name__ == "__main__":
    cli_main()
    #cli_predict()
