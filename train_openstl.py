import cv2
import numpy as np
import os
import pickle
from openstl.utils import show_video_line
from openstl.api import BaseExperiment
from openstl.utils import create_parser
import torch
from torch.utils.data import Dataset
from nets.meteoset import MeteoDataModule, MeteoDataset
import argparse
from openstl.utils import show_video_gif_multiple
from openstl.utils import show_video_line

class MeteoDatasetV2(MeteoDataset):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mean = None
        self.std = None
        
    def __len__(self):
        return super().__len__()
    
    def __getitem__(self, index):
        q,l = super().__getitem__(index)
        return q.unsqueeze(1), l.unsqueeze(1)
    


def main():
    parser = create_parser()
    parser.add_argument('--dataset_path', type=str, required=True, help='dataset path')
    parser.add_argument('--image_size_in', type=int, default=256, help='image size in')
    parser.add_argument('--image_size_out', type=int, default=256, help='image size out')
    args = parser.parse_args()

    dm = MeteoDataModule(
        dataset_path=args.dataset_path,
        batch_size=args.batch_size,
        num_workers=args.num_workers,
        input_sequence_len=args.pre_seq_length,
        output_seuqence_len=args.aft_seq_length,
        image_size_in=args.image_size_in,
        image_size_out=args.image_size_out,
        dataset_cls=MeteoDatasetV2
    )

    train_dl = dm.train_dataloader() 
    val_dl = dm.val_dataloader()
    test_dl = dm.test_dataloader()


    custom_training_config = {
        'total_length': args.pre_seq_length + args.aft_seq_length,
        'batch_size': args.batch_size,
        'val_batch_size': args.batch_size,
        'epoch': 300,
        'lr': 0.001,
        'metrics': ['mse', 'mae'],
        'ex_name': 'custom_exp',
        'dataname': 'custom',
        'in_shape': (args.pre_seq_length, 1, args.image_size_in, args.image_size_in),
        
    }

    custom_model_config = {
    "method": 'PredRNNv2',
    "reverse_scheduled_sampling": 1,
    "r_sampling_step_1": 25000,
    "r_sampling_step_2": 50000,
    "r_exp_alpha": 5000,
    "scheduled_sampling": 1,
    "sampling_stop_iter": 50000,
    "sampling_start_value": 1.0,
    "sampling_changing_rate": 0.00002,
    "num_hidden": '128,128,128,128',
    "filter_size": 5,
    "stride": 1,
    "patch_size": 4,
    "layer_norm": 0,
    "decouple_beta": 0.1,
    "lr": 1e-4,
    "batch_size": 2,
    "sched": 'cosine',
    "noise_type": 'perceptual',
    "warmup_epoch": 0
    }

    config = args.__dict__

    # update the training config
    config.update(custom_training_config)
    # update the model config
    config.update(custom_model_config)

    exp = BaseExperiment(args, dataloaders=(train_dl, val_dl, test_dl))


    print('>'*35 + ' training ' + '<'*35)
    state_dict = torch.load("human_predrnnv2_cos_ep50.pth", map_location='cpu')
    state_dict = {k.replace('module.', ''): v for k, v in state_dict.items()}
    del state_dict['cell_list.0.conv_x.0.weight']
    del state_dict['conv_last.weight']
    exp.method.model.load_state_dict(state_dict, strict=False)
    exp.train()

    
    print('>'*35 + ' testing  ' + '<'*35)
    exp.test()

if __name__ == '__main__':
    main()

