module load cuda/cuda-11.2.0-intel-19.0.4-tn4edsz
module add ffmpeg/4.2.2-intel-19.0.4-getxwtz
export PYTHONPATH=/auto/brno2/home/xjurca08/python_env/usr/local/lib/python3.9/dist-packages/:$PYTHONPATH
export PATH=/auto/brno2/home/xjurca08/python_env/usr/local/bin/:$PATH
export LD_LIBRARY_PATH=/auto/brno2/home/xjurca08/python_env/usr/local/lib:${LD_LIBRARY_PATH}
export HF_DATASETS_CACHE=$SCRATCHDIR/hf_cache


cd /storage/brno2/home/xjurca08/storage/brno2/home/xjurca08/meteonet
git pull
export SIZE=256
export SEQ_LEN=6
#python3.9 train.py --data.batch_size 6 --data.dataset_path /auto/brno12-cerit/home/xjurca08/exported_dataset_1024/ --data.num_workers 6 --trainer.logger lightning.pytorch.loggers.WandbLogger --trainer.default_root_dir results

cp -r /auto/brno12-cerit/home/xjurca08/exported_dataset_1024_hc $SCRATCHDIR/

cp -r /auto/brno12-cerit/home/xjurca08/export_1024_hc_v2 $SCRATCHDIR/
cp -r /auto/brno12-cerit/home/xjurca08/export_1024_hc_v2.zip $SCRATCHDIR/
unzip $SCRATCHDIR/export_1024_hc_v2.zip -d $SCRATCHDIR/

unzip /auto/brno12-cerit/home/xjurca08/export_1024_hc_v4.zip -d $SCRATCHDIR/

unzip /auto/brno12-cerit/home/xjurca08/export_1024_hc_v3.zip -d $SCRATCHDIR/


python3.9 train_unet.py --data.batch_size 4 --data.dataset_path /auto/brno12-cerit/home/xjurca08/exported_dataset_1024/ --data.num_workers 6 --trainer.logger lightning.pytorch.loggers.WandbLogger --trainer.default_root_dir results --trainer.val_check_interval 100
python3.9 diffusion.py --batch_size 24 --dataset_path /auto/brno12-cerit/home/xjurca08/exported_dataset_1024/
python3.9 train_diffusion.py --data.batch_size 24 --data.dataset_path /auto/brno12-cerit/home/xjurca08/exported_dataset_1024/ --data.num_workers 2 --trainer.logger lightning.pytorch.loggers.WandbLogger --trainer.default_root_dir results --trainer.val_check_interval 100 --trainer.log_every_n_steps 2

WANDB_PROJECT="meteo_diffusion" python3.9 train_diffusion.py --data.size 256 --data.batch_size 16 --data.dataset_path $SCRATCHDIR/export_1024_hc_v3 --data.num_workers 4 --trainer.logger lightning.pytorch.loggers.WandbLogger --trainer.log_every_n_steps 2 --data.input_sequence_len 12 --data.output_seuqence_len 6 --model.seq_len_in 12 --model.seq_len_out 6

WANDB_PROJECT="meteo_diffusion" python3.9 train_diffusion.py --data.dataset_path /Users/jjurca/exported_dataset_1024_hc --trainer.accelerator mps --data.batch_size 2 --trainer.logger lightning.pytorch.loggers.WandbLogger --data.input_sequence_len 12 --data.output_seuqence_len 6 --model.seq_len_in 12 --model.seq_len_out 6 --data.size 128

WANDB_PROJECT="meteo_diffusion_3" python3.9 train_diffusion.py --data.size 256 --data.batch_size 16 --data.dataset_path $SCRATCHDIR/export_1024_hc_c15 --data.num_workers 4 --trainer.logger lightning.pytorch.loggers.WandbLogger --trainer.log_every_n_steps 2 --data.input_sequence_len 12 --data.output_seuqence_len 3 --model.seq_len_in 12 --model.seq_len_out 3

CHECKPOINT_PATH="meteo_diffusion/dws1puiy/checkpoints/epoch\=38-step\=122928.ckpt"

# datagenerate
python3.9 train_diffusion.py --checkpoint /storage/brno2/home/xjurca08/storage/brno2/home/xjurca08/meteonet/checkpoints/epoch=11-step=13155.ckpt  --dataset_path /auto/brno12-cerit/home/xjurca08/exported_dataset_1024_hc --batch_size 42 --size 512 --device cuda --num_steps 50                    
python3.9 train_diffusion.py --checkpoint /storage/brno2/home/xjurca08/storage/brno2/home/xjurca08/meteonet/checkpoints/epoch=11-step=13155.ckpt  --dataset_path /auto/brno12-cerit/home/xjurca08/exported_dataset_1024_hc --batch_size 2 --size 512 --device cuda --num_steps 20

WANDB_PROJECT="meteo_srgan" python3 train_srgan.py --data.dataset_path $SCRATCHDIR/export_1024_hc --trainer.accelerator cuda --data.batch_size 512 --trainer.logger lightning.pytorch.loggers.WandbLogger --trainer.precision 16 --model.srresnet_checkpoint meteo_srresnet/dzzsrj7e/checkpoints/epoch\=3-step\=13128.ckpt  --trainer.log_every_n_steps 2 --data.image_size_hr 96
 --model.n_blocks_d 6 --model.n_channels_d 32
WANDB_PROJECT="meteo_srresnet" python3 train_srresnet.py  --data.dataset_path /auto/brno12-cerit/home/xjurca08/export_1024_hc --trainer.accelerator cuda --data.batch_size 512 --trainer.logger lightning.pytorch.loggers.WandbLogger  --trainer.precision 16

BATCH_SIZE="512" DATA_FOLDER="/auto/brno12-cerit/home/xjurca08/export_1024_hc" python3 train_srgan.py
CROP_SIZE=128 SCALING_FACTOR=4 BATCH_SIZE="512" DATA_FOLDER="/auto/brno12-cerit/home/xjurca08/export_1024_hc" python3 train_srgan.py

CROP_SIZE=128 SCALING_FACTOR=4 BATCH_SIZE="32" DATA_FOLDER="/Users/jjurca/exported_dataset_1024_hc" python3 train_srresnet.py

GENERATOR_FREEZE=5 SCALING_FACTOR=4 BATCH_SIZE="512" DATA_FOLDER="$SCRATCHDIR/export_1024_hc" python3 train_srgan.py

LR_DECAY=0.90 GENERATOR_FREEZE=2 SCALING_FACTOR=4 BATCH_SIZE="512" DATA_FOLDER="$SCRATCHDIR/export_1024_hc" python3 train_srgan.py