# Copyright The Lightning AI team.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""MNIST autoencoder example.

To run: python autoencoder.py --trainer.max_epochs=50

"""
from os import path
from typing import Optional, Tuple

import torch
import torch.nn.functional as F
from torch import nn
from torch.utils.data import DataLoader, random_split

from lightning.pytorch import callbacks, cli_lightning_logo, LightningDataModule, LightningModule, Trainer
from lightning.pytorch.cli import LightningCLI
from lightning.pytorch.demos.mnist_datamodule import MNIST
from lightning.pytorch.utilities import rank_zero_only
from lightning.pytorch.utilities.imports import _TORCHVISION_AVAILABLE
from lightning.pytorch.callbacks import ModelCheckpoint
from lightning.pytorch.callbacks import Callback

import torchvision
from torchvision import transforms
from torchvision.utils import save_image

from nets.vnet import VNet
import json
import glob
from PIL import Image
import os
import numpy as np
import argparse
import random
Image.MAX_IMAGE_PIXELS = 933120000
SEQ_LEN = 16

class MeteoNet(LightningModule):
    def __init__(self, learning_rate: float = 0.0001):
        super().__init__()
        self.save_hyperparameters()
        self.model = VNet(elu=False, nll=True, inCH=int(SEQ_LEN/2), outCH=int(SEQ_LEN/2))

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        return self._common_step(batch, batch_idx, "train")[0]

    def validation_step(self, batch, batch_idx):
        loss, output = self._common_step(batch, batch_idx, "val")
        # set dimensionaly same as batch
        if batch_idx == 0:
            output = output.view(batch[1].shape)
            imgs = []
            # extract first image from output 
            output = output[0]
            imgs = []
            for i, img in enumerate(output):
                img = img.unsqueeze(0)
                os.makedirs("output", exist_ok=True)
                img= img.detach().cpu()
                img = img.squeeze(0)
                img = img.numpy()
                img = np.array(img*255, dtype=np.uint8)
                image = Image.fromarray(img)
                imgs.append(image)
            gts = []
            for i, img in enumerate(batch[1][0]):
                img = img.unsqueeze(0)
                os.makedirs("output", exist_ok=True)
                img= img.detach().cpu()
                img = img.squeeze(0)
                img = img.numpy()
                img = np.array(img*255, dtype=np.uint8)
                image = Image.fromarray(img)
                gts.append(image)
            self.logger.log_image(key="samples", images=imgs + gts, step=self.global_step, caption=[f"output {i}" for i in range(len(imgs))] + [f"gt {i}" for i in range(len(gts))])
        return loss

    def test_step(self, batch, batch_idx):
        loss, output = self._common_step(batch, batch_idx, "test")
        return loss

    def predict_step(self, batch, batch_idx, dataloader_idx=None):
        q, _ = batch
        return self(q)

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.hparams.learning_rate)


    def _common_step(self, batch, batch_idx, stage: str):
        q, gt = batch
        output = self(q)
        gt = gt.view(gt.numel())
        loss = F.mse_loss(gt,output )
        self.log(f"{stage}_loss", loss, on_step=True)
        return loss, output

class MeteoDataset(torch.utils.data.Dataset):
    def __init__(self, root, portion=0.75, sequence_length=32):
        self.root = root
        self.sequence_length = sequence_length
        tiles_dirs = glob.glob(path.join(root, "*"))
        tiles = {} 
        for tile_dir in tiles_dirs:
            if path.isdir(tile_dir):
                tile = path.basename(tile_dir)
                tiles[tile] = []
                for file in glob.glob(path.join(tile_dir, "*.png")):
                    tiles[tile].append(file)
                tiles[tile].sort()
                
        self.sequences = []
        diference = 600
        for tile in tiles:
            for i, file in enumerate(tiles[tile]):
                sequence = []
                file_timestamp = int(path.basename(file).split(".")[0])
                for j, file2 in enumerate(tiles[tile][i+1:]):
                    file2_timestamp = int(path.basename(file2).split(".")[0])
                    if file2_timestamp == file_timestamp+(j+1)*diference:
                        sequence.append(file2)
                    else:
                        break
                    if len(sequence) == sequence_length:
                        break
                if len(sequence) == sequence_length:
                    self.sequences.append(tuple(sequence))
        if portion < 0:
            self.data = self.sequences[int(len(self.sequences)*(1+portion)):]
        else:
            self.data = self.sequences[:int(len(self.sequences)*portion)]
        random.shuffle(self.data)

    def cut_dataset(self, n,first=True):
        if first:
            self.data = self.data[:n]
        else:
            self.data = self.data[n:]
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        sequence = self.data[index]
        query = []
        gt = []
        for i, file in enumerate(sequence):
            with Image.open(file) as img:
                img = img.convert("L")
                img = img.resize((256, 256))
                img = np.array(img, dtype=np.float32)
                img = torch.from_numpy(img)
                img = img.float() / 255.0
                if i >= self.sequence_length/2:
                    gt.append(img)
                else:
                    query.append(img)
        query = torch.stack(query).unsqueeze(0)
        gt = torch.stack(gt)
        return query, gt

class MeteoDataModule(LightningDataModule):
    def __init__(self, dataset_path: str, batch_size: int = 32, num_workers: int = 1):
        super().__init__()
        self.test = MeteoDataset(root=dataset_path, portion=-0.25, sequence_length=SEQ_LEN)
        self.train =  MeteoDataset(root=dataset_path, portion=0.75, sequence_length=SEQ_LEN)
        self.val = MeteoDataset(root=dataset_path, portion=-0.25, sequence_length=SEQ_LEN)
        self.val.cut_dataset(10, first=True)
        self.test.cut_dataset(10, first=False)
        self.batch_size = batch_size
        self.num_workers = num_workers

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, num_workers=self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.batch_size, num_workers=self.num_workers)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, num_workers=self.num_workers)

    def predict_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, num_workers=self.num_workers)


def cli_main():
    checkpoint_callback = ModelCheckpoint(
                dirpath='./checkpoints',
                monitor='val_loss',
                save_top_k=2
            )
    cli = LightningCLI(
        MeteoNet,
        MeteoDataModule,
        seed_everything_default=1234,
        run=False,  # used to de-activate automatic fitting.
        trainer_defaults={"callbacks": [checkpoint_callback], "max_epochs": 10},
        save_config_kwargs={"overwrite": True},
    )
    cli.trainer.fit(cli.model, datamodule=cli.datamodule, )
    cli.trainer.test(datamodule=cli.datamodule)
    predictions = cli.trainer.predict(datamodule=cli.datamodule)
    print(predictions[0])


if __name__ == "__main__":
    cli_lightning_logo()
    cli_main()
