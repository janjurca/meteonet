from typing import Optional
import torch
from torch import nn
from torch.utils.data import DataLoader, default_collate
from torch.nn import init

from lightning.pytorch import LightningDataModule, LightningModule
from lightning.pytorch.cli import LightningCLI
from lightning.pytorch.callbacks import ModelCheckpoint, LearningRateMonitor

from fastprogress import progress_bar

from PIL import Image
import os
from diffusers import UNet2DModel, DDIMScheduler
from nets.meteoset import DiffusionMeteoDataset
import fastcore.all as fc
import numpy as np
from functools import partial
import matplotlib.pyplot as plt 
from tqdm import tqdm
import uuid
from nets.unet3d import UNet3DModel

Image.MAX_IMAGE_PIXELS = 933120000

class CustomExponentialLR(torch.optim.lr_scheduler.ExponentialLR):
    def __init__(self, optimizer, gamma, min_lr, last_epoch=-1):
        self.min_lr = min_lr
        super().__init__(optimizer, gamma, last_epoch)

    def get_lr(self):
        # Get the exponentially decayed learning rates
        lrs = super().get_lr()
        return [max(lr, self.min_lr) for lr in lrs]


class MeteoNet(LightningModule):
    def __init__(self, learning_rate: float = 0.0001, seq_len_in: int = 12, seq_len_out: int = 6, model_str: str = "unet2D"):
        super().__init__()
        self.save_hyperparameters()
        if model_str == "unet2D":
            self.model = UNet2DModel(
                in_channels=seq_len_in + seq_len_out,
                out_channels=seq_len_out,
                layers_per_block=2,  # how many ResNet layers to use per UNet block
                block_out_channels=(64, 64, 128, 128, 256, 256),  # the number of output channels for each UNet block
                down_block_types=(
                    "DownBlock2D",  # a regular ResNet downsampling block
                    "DownBlock2D",
                    "AttnDownBlock2D",
                    "DownBlock2D",
                    "AttnDownBlock2D",  # a ResNet downsampling block with spatial self-attention
                    "DownBlock2D",
                ),
                up_block_types=(
                    "UpBlock2D",  # a regular ResNet upsampling block
                    "AttnUpBlock2D",  # a ResNet upsampling block with spatial self-attention
                    "UpBlock2D",
                    "AttnUpBlock2D",
                    "UpBlock2D",
                    "UpBlock2D",
                ),
            )
        elif model_str == "unet3D":
            self.model = UNet3DModel(
                in_channels=seq_len_in + seq_len_out,
                out_channels=seq_len_out,
                layers_per_block=2,  # how many ResNet layers to use per UNet block
                block_out_channels=(64, 64, 128, 128, 256, 256),  # the number of output channels for each UNet block
                down_block_types=(
                    "DownBlock3D",  # a regular ResNet downsampling block
                    "DownBlock3D",
                    "AttnDownBlock3D",
                    "DownBlock3D",
                    "AttnDownBlock3D",  # a ResNet downsampling block with spatial self-attention
                    "DownBlock3D",
                ),
                up_block_types=(
                    "UpBlock3D",  # a regular ResNet upsampling block
                    "AttnUpBlock3D",  # a ResNet upsampling block with spatial self-attention
                    "UpBlock3D",
                    "AttnUpBlock3D",
                    "UpBlock3D",
                    "UpBlock3D",
                ),
            )
        else:
            raise ValueError("Model not supported")


        self.init_ddpm(self.model)
        self.loss = nn.MSELoss(reduction="mean")
        # load checkpoint if set env
        if os.environ.get("CHECKPOINT_PATH"):
            self.load_state_dict(torch.load(os.environ.get("CHECKPOINT_PATH"), map_location=torch.device('cpu'))["state_dict"])



    def forward(self, x):
        out = self.model(x[0], x[1]).sample
        return out

    @staticmethod
    def init_ddpm(model):
        for o in model.down_blocks:
            for p in o.resnets:
                p.conv2.weight.data.zero_()
                for p in fc.L(o.downsamplers): init.orthogonal_(p.conv.weight)

        for o in model.up_blocks:
            for p in o.resnets: p.conv2.weight.data.zero_()

        model.conv_out.weight.data.zero_()


    def training_step(self, batch, batch_idx):
        return self._common_step(batch, batch_idx, "train")[0]

    def validation_step(self, batch, batch_idx):
        loss, output = self._common_step(batch, batch_idx, "val")

        if batch_idx == 0: # DISABLED
            xt, t, ε = batch
            xt = xt[:10,:-self.hparams.seq_len_out,...]
            images = self.sample_images(xt)
            # extract first image from output 
                      
            self.logger.log_image(key="samples", images=images, step=self.global_step)
        return loss
  
    def test_step(self, batch, batch_idx):
        loss, output = self._common_step(batch, batch_idx, "test")
        return loss

    def configure_optimizers(self):
        opt = torch.optim.Adam(self.parameters(), lr=self.hparams.learning_rate)
        ret = {
            "optimizer": opt,
            'lr_scheduler':{
                "scheduler": CustomExponentialLR(opt, gamma=0.9999, min_lr=1e-7),
                "interval": "step",
            }
        }
        return ret

    def _common_step(self, batch, batch_idx, stage: str):
        output = self(batch)
        loss = self.loss(output, *batch[2:])
        self.log(f"{stage}_loss", loss, prog_bar=True)
        return loss, output

    @torch.no_grad()
    def diffusers_sampler(self, past_frames, sched, **kwargs):
        "Using Diffusers built-in samplers"
        self.model.eval()
        device = next(self.model.parameters()).device
        past_frames = past_frames.to(device)
        new_frames = torch.randn_like(past_frames[:,-self.hparams.seq_len_out:], dtype=past_frames.dtype, device=device)
        preds = []
        pbar = progress_bar(sched.timesteps, leave=False)
        for t in pbar:
            pbar.comment = f"DDIM Sampler: frame {t}"
            noise = self.model(torch.cat([past_frames, new_frames], dim=1), t).sample
            new_frames = sched.step(noise, t, new_frames, **kwargs).prev_sample
            preds.append(new_frames.float().cpu())
        return preds[-1]

    def ddim_sampler(self, steps=50, eta=1.):
        "DDIM sampler, faster and a bit better than the built-in sampler"
        ddim_sched = DDIMScheduler()
        ddim_sched.set_timesteps(steps)
        return partial(self.diffusers_sampler, sched=ddim_sched, eta=eta)

    def to_wandb_image(self, img):
        "Stack the images horizontally"
        return torch.cat(img.split(1), dim=-1).cpu().numpy()

    def sample_images(self, xt, steps=50):
        "Sample and log images to W&B"
        samples = self.ddim_sampler(steps=steps)(xt.clone())
        frames = torch.cat([xt.to(samples[-1].device), samples], dim=1)
        # Convert torch tensor to numpy
        frames_np = frames.cpu().numpy()
        ret = []
        for i,images in enumerate(frames_np):
            image = None
            for i, subimage in enumerate(images):
                subimage = subimage / 2
                subimage = subimage + 0.5
                subimage = subimage * 255
                if np.min(subimage) < 0:
                    subimage = np.clip(subimage, 0, 255)
                if image is None:
                    image = subimage
                else:
                    image = np.concatenate((image, subimage), axis=1)
            ret.append(image)
        return ret

    def log_images(self, xt, steps=50):
        import matplotlib.pyplot as plt
        frames_np = self.sample_images(xt, steps=steps)
        fig, axs = plt.subplots( len(frames_np) ,1, figsize=(15, 15))
        for i,image in enumerate(frames_np):
            axs[i].imshow(image, cmap='gray')
        plt.show()


"""We will use the same noisify function as the one from the fastai course"""
betamin,betamax,n_steps = 0.0001,0.02, 1000
beta = torch.linspace(betamin, betamax, n_steps)
alpha = 1.-beta
alphabar = alpha.cumprod(dim=0)
sigma = beta.sqrt()

def noisify_ddpm(x0):
    "Noise by ddpm"
    device = x0.device
    n = len(x0)
    t = torch.randint(0, n_steps, (n,), dtype=torch.long)
    ε = torch.randn(x0.shape, device=device)
    ᾱ_t = alphabar[t].reshape(-1, 1, 1, 1).to(device)
    xt = ᾱ_t.sqrt()*x0 + (1-ᾱ_t).sqrt()*ε
    return xt, t.to(device), ε

"""We wrap the noisify func and apply it to the last frame of the sequence. We refactor this in the `NoisifyDataloader`"""

def noisify_last_frame(frames, noise_func, n_frames):
    "Noisify the last frame of a sequence"
    past_frames = frames[:,:-n_frames]
    last_frame  = frames[:,-n_frames:]
    noise, t, e = noise_func(last_frame)
    return torch.cat([past_frames, noise], dim=1), t, e

class NoisifyCollate:
    def __init__(self, noise_func, n_frames):
        self.noise_func = noise_func
        self.n_frames = n_frames

    def __call__(self, b):
        return noisify_last_frame(default_collate(b), self.noise_func, n_frames=self.n_frames)



class NoisifyDataloader(DataLoader):
    """Noisify the last frame of a dataloader by applying
    a noise function, after collating the batch"""
    def __init__(self, dataset, *args, noise_func=noisify_ddpm,n_frames , **kwargs):
        super().__init__(dataset, *args, collate_fn=NoisifyCollate(noise_func, n_frames), **kwargs)

class MeteoDataModule(LightningDataModule):
    def __init__(self, dataset_path: str, batch_size: int = 32, num_workers: int = 1, size: int = 512, input_sequence_len:int=12, output_sequence_len:int=6, is3D:bool=False):
        super().__init__()
        self.test = DiffusionMeteoDataset(root=dataset_path, portion=-0.25, image_size_in=size, image_size_out=size, input_sequence_len=input_sequence_len, output_sequence_len=output_sequence_len,is3D=is3D)
        self.train =  DiffusionMeteoDataset(root=dataset_path, portion=1, image_size_in=size, image_size_out=size, input_sequence_len=input_sequence_len, output_sequence_len=output_sequence_len,is3D=is3D)
        self.val = DiffusionMeteoDataset(root=dataset_path, portion=-0.25, image_size_in=size, image_size_out=size, input_sequence_len=input_sequence_len, output_sequence_len=output_sequence_len,is3D=is3D)
        self.val.cut_dataset(10, first=True)
        self.test.cut_dataset(10, first=False)
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.is3D = is3D
        self.input_sequence_len = input_sequence_len
        self.output_sequence_len = output_sequence_len
        if self.is3D:
            self.input_sequence_len = 1
            self.output_sequence_len = 1

    def train_dataloader(self):
        return NoisifyDataloader(self.train, batch_size=self.batch_size, num_workers=self.num_workers, n_frames=self.output_sequence_len)

    def val_dataloader(self):
        return NoisifyDataloader(self.val, batch_size=self.batch_size, num_workers=self.num_workers, n_frames=self.output_sequence_len)

    def test_dataloader(self):
        return NoisifyDataloader(self.test, batch_size=self.batch_size, num_workers=self.num_workers, n_frames=self.output_sequence_len)


def cli_main():
    #checkpoint_callback = ModelCheckpoint(
    #            dirpath='./checkpoints',
    #            monitor='val_loss',
    #            save_top_k=2
    #        )
    lr_monitor = LearningRateMonitor(logging_interval='step')
    cli = LightningCLI(
        MeteoNet,
        MeteoDataModule,
        seed_everything_default=1234,
        run=False,  # used to de-activate automatic fitting.
        trainer_defaults={"max_epochs": 100, "precision": "16-mixed", "callbacks": [lr_monitor]},
        save_config_kwargs={"overwrite": True},
    )
    cli.trainer.fit(cli.model, datamodule=cli.datamodule, )
    #cli.trainer.test(datamodule=cli.datamodule)

def cli_predict():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', type=str, required=True, help='Path to the checkpoint')
    parser.add_argument('--dataset_path', type=str, required=True, help='Path to the dataset')
    parser.add_argument('--batch_size', type=int, default=1, help='Batch size')
    parser.add_argument('--size', type=int, default=512, help='Image size')
    parser.add_argument('--device', type=str, default='cpu', help='Device')
    parser.add_argument('--num_steps', type=int, default=100, help='Number of steps')
    parser.add_argument('--n_frames', type=int, default=6, help='Number of steps')
    args = parser.parse_args()
    dm = MeteoDataModule(dataset_path=args.dataset_path, batch_size=args.batch_size, num_workers=1, size=args.size)
    xt, t, ε = next(iter(dm.val_dataloader()))
    xt = xt[:10,:-args.n_frames,...]

    model = MeteoNet.load_from_checkpoint(args.checkpoint, map_location=torch.device('cpu'))
    model = model.to(args.device)
    xt = xt.to(args.device)
    model.log_images(xt, steps=args.num_steps)

def generate_dataset():
    import argparse
    import matplotlib.pyplot as plt

    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', type=str, required=True, help='Path to the checkpoint')
    parser.add_argument('--dataset_path', type=str, required=True, help='Path to the dataset')
    parser.add_argument('--batch_size', type=int, default=1, help='Batch size')
    parser.add_argument('--size', type=int, default=512, help='Image size')
    parser.add_argument('--device', type=str, default='cpu', help='Device')
    parser.add_argument('--num_steps', type=int, default=50, help='Number of steps')
    parser.add_argument('--save_to', type=str, default='generated', help='Path to save the images')
    args = parser.parse_args()
    dataset = DiffusionMeteoDataset(root=args.dataset_path, image_size_in=args.size, image_size_out=args.size, return_original=True)

    os.makedirs(args.save_to, exist_ok=True)
    model = MeteoNet.load_from_checkpoint(args.checkpoint, map_location=torch.device('cpu'))
    model = model.to(args.device)

    for x, files in tqdm(dataset):
        filename = files[-1].split("/")[-2] + "_" + os.path.basename(files[-1]).split(".")[0]  
        if os.path.isfile(os.path.join(args.save_to, f"{filename}_prediction.png")) and os.path.isfile(os.path.join(args.save_to, f"{filename}_original.png")):
            print(f"Skipping {filename}")
            continue

        xt = x[:-1,...].unsqueeze(0)
        xt = xt.to(args.device)

        samples = model.ddim_sampler(steps=args.num_steps)(xt)
        frames = torch.cat([xt.to(samples[-1].device), samples], dim=1)
        # Convert torch tensor to numpy
        frames_np = frames.cpu().numpy().squeeze()
        
        prediction = frames_np[7,...]
        prediction = np.clip(prediction, 0, None)

        prediction = prediction * 255
        prediction = prediction.astype(np.uint8)
        prediction = Image.fromarray(prediction)
        original = Image.open(files[-1])
        original = original.convert("L")

        original.save(os.path.join(args.save_to, f"{filename}_original.png"))
        prediction.save(os.path.join(args.save_to, f"{filename}_prediction.png"))

    
if __name__ == "__main__":
    cli_main()
    #cli_predict()
    #generate_dataset()
