import torch
from torch import nn

from lightning.pytorch import LightningModule
from lightning.pytorch.cli import LightningCLI

from models import Generator, Discriminator, TruncatedVGG19
import glob
from os import path
from PIL import Image, ImageFilter
import random
from torchvision import transforms
from torch.utils.data import DataLoader
from lightning.pytorch import LightningDataModule
import numpy as np
import torch.nn.functional as F
import os
from nets.meteoset import SRMeteoDataModule,SRMeteoDataset

class MeteoSR(LightningModule):
    def __init__(self, learning_rate: float = 0.0001, scaling_factor = 2, beta = 0.001,grad_clip = None, srresnet_checkpoint = None,
                 large_kernel_size_g: int = 9, small_kernel_size_g: int = 3, n_channels_g: int = 64, n_blocks_g: int = 16,
                 kernel_size_d: int = 3, n_channels_d: int = 64, n_blocks_d: int = 8, fc_size_d: int = 1024,
                 vgg19_i = 5, vgg19_j = 4
                 ):
        super().__init__()
        self.save_hyperparameters()
        self.generator = Generator(large_kernel_size=large_kernel_size_g,
                              small_kernel_size=small_kernel_size_g,
                              n_channels=n_channels_g,
                              n_blocks=n_blocks_g,
                              scaling_factor=scaling_factor)
        
        if srresnet_checkpoint and os.path.exists(srresnet_checkpoint):
            srresnet = torch.load(srresnet_checkpoint, map_location=torch.device('cpu') )
            self.generator.load_state_dict({x.replace("model.","net."):y for x, y in srresnet["state_dict"].items()})
            print("Loaded SRResNet checkpoint")

        self.discriminator = Discriminator(kernel_size=kernel_size_d,
                                      n_channels=n_channels_d,
                                      n_blocks=n_blocks_d,
                                      fc_size=fc_size_d)

        self.truncated_vgg19 = TruncatedVGG19(i=vgg19_i, j=vgg19_j)
        self.truncated_vgg19.eval()

        self.adversarial_loss_criterion = nn.BCEWithLogitsLoss()
        self.content_loss_criterion = nn.MSELoss()
        self.automatic_optimization = False

    def forward(self, x):
        x = self.generator(x)
        return x


    def training_step(self, batch, batch_idx):
        self._common_step(batch, batch_idx, "train")

    def validation_step(self, batch, batch_idx):
        lr_imgs, hr_imgs = batch
        sr_imgs = self(lr_imgs)
        if batch_idx == 0:
            images = []
            for sr, lr, hr in zip(sr_imgs, lr_imgs, hr_imgs):
                sr, lr, hr = sr.cpu().detach(), lr.cpu().detach(), hr.cpu().detach()
                lr_image = transforms.ToPILImage()(lr)
                sr_image = transforms.ToPILImage()(sr)
                lr_image = lr_image.resize(sr_image.size, Image.BICUBIC)
                lr = transforms.ToTensor()(lr_image)
                
                grid = torch.cat((lr, sr, hr), -1)
                images.append(grid)
            self.logger.log_image(key="sr", images=images, step=self.global_step)

    def configure_optimizers(self):
        lr = self.hparams.learning_rate
        opt_g = torch.optim.Adam(params=filter(lambda p: p.requires_grad, self.generator.parameters()),lr=lr)
        opt_d = torch.optim.Adam(params=filter(lambda p: p.requires_grad, self.discriminator.parameters()),lr=lr)
        return [opt_g, opt_d], []

    @staticmethod   
    def clip_gradient(optimizer, grad_clip):
        """
        Clips gradients computed during backpropagation to avoid explosion of gradients.

        :param optimizer: optimizer with the gradients to be clipped
        :param grad_clip: clip value
        """
        for group in optimizer.param_groups:
            for param in group['params']:
                if param.grad is not None:
                    param.grad.data.clamp_(-grad_clip, grad_clip)

    def _common_step(self, batch, batch_idx, stage: str):
        optimizer_g, optimizer_d = self.optimizers()
        grad_clip = self.hparams.grad_clip
        lr_imgs, hr_imgs = batch
        
        self.toggle_optimizer(optimizer_g)
        sr_imgs = self(lr_imgs)

        sr_imgs_3_channels = sr_imgs.expand(-1, 3, -1, -1)
        hr_imgs_3_channels = hr_imgs.expand(-1, 3, -1, -1)

        sr_imgs_in_vgg_space = self.truncated_vgg19(sr_imgs_3_channels)
        hr_imgs_in_vgg_space = self.truncated_vgg19(hr_imgs_3_channels).detach()

        # Discriminate super-resolved (SR) images
        sr_discriminated = self.discriminator(sr_imgs)  # (N)
        
        # Calculate the Perceptual loss
        content_loss = self.content_loss_criterion(sr_imgs_in_vgg_space, hr_imgs_in_vgg_space)
        adversarial_loss = self.adversarial_loss_criterion(sr_discriminated, torch.ones_like(sr_discriminated))
        perceptual_loss = content_loss + self.hparams.beta * adversarial_loss
        perceptual_loss = self.adversarial_loss_criterion(sr_discriminated, torch.ones_like(sr_discriminated))

        self.log("perceptual_loss", perceptual_loss, prog_bar=True)
        
        self.manual_backward(perceptual_loss)
        if grad_clip is not None:
            self.clip_gradient(optimizer_g, grad_clip)
        optimizer_g.step()
        optimizer_g.zero_grad()
        self.untoggle_optimizer(optimizer_g)


        # Train discriminator
        self.toggle_optimizer(optimizer_d)
        hr_discriminated = self.discriminator(hr_imgs)
        sr_discriminated = self.discriminator(sr_imgs.detach())
        adversarial_loss = self.adversarial_loss_criterion(sr_discriminated, torch.zeros_like(sr_discriminated)) + \
                           self.adversarial_loss_criterion(hr_discriminated, torch.ones_like(hr_discriminated))
        self.log("d_loss", adversarial_loss, prog_bar=True)   

        self.manual_backward(adversarial_loss)
        if grad_clip is not None:
            self.clip_gradient(optimizer_d, grad_clip)
        
        optimizer_d.step()
        optimizer_d.zero_grad()
        self.untoggle_optimizer(optimizer_d)

        


def cli_main():
    cli = LightningCLI(
        MeteoSR,
        SRMeteoDataModule,
        seed_everything_default=1234,
        run=False,  # used to de-activate automatic fitting.
        trainer_defaults={"max_epochs": 10},
        save_config_kwargs={"overwrite": True},
    )
    cli.trainer.fit(cli.model, datamodule=cli.datamodule, )
    #cli.trainer.test(datamodule=cli.datamodule)


def cli_show_data():
    cli = LightningCLI(
        MeteoSR,
        SRMeteoDataModule,
        seed_everything_default=1234,
        run=False,  # used to de-activate automatic fitting.
        trainer_defaults={"max_epochs": 100},
        save_config_kwargs={"overwrite": True},
    )
    for batch in cli.datamodule.train_dataloader():
        lr_imgs, hr_imgs = batch
        for lr, hr in zip(lr_imgs, hr_imgs):
            lr, hr = lr.cpu().detach(), hr.cpu().detach()
            lr_image = transforms.ToPILImage()(lr)
            hr_image = transforms.ToPILImage()(hr)
            lr_image = lr_image.resize(hr_image.size, Image.BICUBIC)
            lr = transforms.ToTensor()(lr_image)
            grid = torch.cat((lr, hr), -1)
            grid = grid.unsqueeze(0)
            grid_image = transforms.ToPILImage()(grid[0])
            grid_image.show()
        break

if __name__ == "__main__":
    #cli_main()
    cli_show_data()
