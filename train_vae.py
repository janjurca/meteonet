import torch
from lightning.pytorch import LightningModule
from lightning.pytorch.cli import LightningCLI

from nets.meteoset import VAEMeteoDataModule
import torch
from diffusers.models import AutoencoderKL
from ldm.util import instantiate_from_config
from PIL import Image
import numpy as np
from ldm.modules.losses import LPIPSWithDiscriminator

class AutoencoderKLCompVis(LightningModule):
    def __init__(self,
                 image_key="image",
                 lr=5e-5,
                 ):
        super().__init__()
        self.save_hyperparameters()
        self.learning_rate = lr
        self.image_key = image_key
        self.vae = AutoencoderKL.from_pretrained("stabilityai/sd-vae-ft-mse")
        self.loss = instantiate_from_config({
            "target": "ldm.modules.losses.LPIPSWithDiscriminator",
            "params": {
                "disc_start": 0,
                "kl_weight": 0.000001,
                "disc_weight": 0.5,
            }
        })
        #freeze the vae
        for param in self.vae.parameters():
            param.requires_grad = False
        #unfree 3 last layers in vaes decoder
        for param in self.vae.decoder.parameters():
            param.requires_grad = True
        

        self.automatic_optimization = False

    def encode(self, x):
        h = self.vae.encode(x)
        return h

    def decode(self, z):
        #z = self.post_quant_conv(z)
        dec = self.vae.decode(z)
        return dec

    def forward(self, inp, sample_posterior=True):
        posterior = self.encode(inp).latent_dist
        if sample_posterior:
            z = posterior.sample()
        else:
            z = posterior.mode()
        dec = self.decode(z).sample
        return dec, posterior

    def get_input(self, batch, k):
        x = batch[0]
        if len(x.shape) == 3:
            x = x[..., None]
        #x = x.permute(0, 3, 1, 2).to(memory_format=torch.contiguous_format).float()
        return x

    def training_step(self, batch, batch_idx):
        inputs = self.get_input(batch, self.image_key)
        reconstructions, posterior = self(inputs)
        opt1, opt2 = self.optimizers()

        optimizer_idx = 0
        self.toggle_optimizer(opt1)
        # train encoder+decoder+logvar
        aeloss, log_dict_ae = self.loss(inputs, reconstructions, posterior, optimizer_idx, self.global_step,
                                        last_layer=self.get_last_layer(), split="train")
        
        self.log("aeloss", aeloss, prog_bar=True, logger=True, on_step=True, on_epoch=True)
        self.log_dict(log_dict_ae, prog_bar=False, logger=True, on_step=True, on_epoch=False)
        aeloss.backward()
        opt1.step()
        opt1.zero_grad()
        self.untoggle_optimizer(opt1)

        # train the discriminator
        optimizer_idx = 1
        self.toggle_optimizer(opt2)
        discloss, log_dict_disc = self.loss(inputs, reconstructions, posterior, optimizer_idx, self.global_step,
                                            last_layer=self.get_last_layer(), split="train")

        self.log("discloss", discloss, prog_bar=True, logger=True, on_step=True, on_epoch=True)
        self.log_dict(log_dict_disc, prog_bar=False, logger=True, on_step=True, on_epoch=False)
        discloss.backward()
        opt2.step()
        opt2.zero_grad()
        self.untoggle_optimizer(opt2)

    def validation_step(self, batch, batch_idx):
        inputs = self.get_input(batch, self.image_key)
        reconstructions, posterior = self(inputs)
        aeloss, log_dict_ae = self.loss(inputs, reconstructions, posterior, 0, self.global_step,
                                        last_layer=self.get_last_layer(), split="val")

        discloss, log_dict_disc = self.loss(inputs, reconstructions, posterior, 1, self.global_step,
                                            last_layer=self.get_last_layer(), split="val")

        self.log("val/rec_loss", log_dict_ae["val/rec_loss"])
        self.log_dict(log_dict_ae)
        self.log_dict(log_dict_disc)
        if batch_idx == 0:
            self.log_images(batch)

        return self.log_dict

    def configure_optimizers(self):
        lr = self.learning_rate
        opt_ae = torch.optim.Adam(params=filter(lambda p: p.requires_grad, self.vae.parameters()),lr=lr, betas=(0.5, 0.9))
        opt_disc = torch.optim.Adam(params=filter(lambda p: p.requires_grad, self.loss.discriminator.parameters()),lr=lr, betas=(0.5, 0.9))

        return [opt_ae, opt_disc], []

    def get_last_layer(self):
        return self.vae.decoder.conv_out.weight

    @torch.no_grad()
    def log_images(self, batch, only_inputs=False, **kwargs):
        log = dict()
        x = self.get_input(batch, self.image_key)
        x = x.to(self.device)
        if not only_inputs:
            xrec, posterior = self(x)
            log["reconstructions"] = xrec
        log["inputs"] = x
        # cat to one image
        images = []
        for  reconstuction, inp in zip(log["reconstructions"], log["inputs"]):
            grid = torch.cat((inp, reconstuction), -1)
            z = grid.cpu().detach().numpy()
            z = z.squeeze()
            z = z.transpose(1,2,0)
            z = z * 255.0
            z = z.astype(np.uint8)
            z = Image.fromarray(z)
            z = z.convert("L")
            images.append(z)
        self.logger.log_image(key="samples", images=images, step=self.global_step)
        return log

def cli_main():
    cli = LightningCLI(
        AutoencoderKLCompVis,
        VAEMeteoDataModule,
        seed_everything_default=1234,
        run=False,  # used to de-activate automatic fitting.
        trainer_defaults={"max_epochs": 1000},
        save_config_kwargs={"overwrite": True},
    )
    cli.trainer.fit(cli.model, datamodule=cli.datamodule, )
    #cli.trainer.test(datamodule=cli.datamodule)


if __name__ == "__main__":
    cli_main()
    #cli_predict()
