import torch
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torchvision import transforms
from PIL import Image
import sys 
import numpy as np
# Define the noise functions
betamin, betamax, n_steps = 0.0001, 0.02, 1000
beta = torch.linspace(betamin, betamax, n_steps)
alpha = 1. - beta
alphabar = alpha.cumprod(dim=0)
sigma = beta.sqrt()

def noisify_ddpm(x0):
    "Noise by ddpm"
    device = x0.device
    n = len(x0)
    t = torch.randint(0, n_steps, (n,), dtype=torch.long)
    t = torch.full((n,), int(sys.argv[2]), dtype=torch.long)
    ε = torch.randn(x0.shape, device=device)
    ᾱ_t = alphabar[t].reshape(-1, 1, 1, 1).to(device)
    xt = ᾱ_t.sqrt() * x0 + (1 - ᾱ_t).sqrt() * ε
    return xt, t.to(device), ε

# Load and preprocess the image
def load_and_preprocess_image(image_path):
    transform = transforms.Compose([transforms.ToTensor()])
    image = Image.open(image_path)
    image = torch.from_numpy(np.array(image, dtype=np.float32))
    image = image.unsqueeze(0)
    image = image / 255.0   
    return image.unsqueeze(0)  # Add batch dimension

# Apply noise to the image
def apply_noise_to_image(image, noise_func):
    noisy_image, _, _ = noise_func(image)
    return noisy_image

# Load the image
image_path = sys.argv[1]
original_image = load_and_preprocess_image(image_path)

# Apply noise to the image
noisy_image = apply_noise_to_image(original_image, noisify_ddpm)

# Plot the original and noisy images
fig, axs = plt.subplots(1, 2, figsize=(10, 5))
axs[0].imshow(original_image[0].permute(1, 2, 0).cpu().numpy())
axs[0].set_title('Original Image')
axs[0].axis('off')
axs[1].imshow(noisy_image[0].permute(1, 2, 0).cpu().numpy())
axs[1].set_title('Noisy Image')
axs[1].axis('off')
plt.show()
